package com.bigdata.sacw.reconciliation.controller;

import com.bigdata.sacw.reconciliation.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    private IUserService a;

    @GetMapping("/test")
    public String test (){
        return a.selectById(1).toString();
    }

    @GetMapping("/user/{id}")
    public String t(@PathVariable  Long id){
        return a.selectById(id) + "1111111111111";
    }
}
