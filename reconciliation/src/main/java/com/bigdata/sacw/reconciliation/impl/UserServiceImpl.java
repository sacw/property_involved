package com.bigdata.sacw.reconciliation.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bigdata.sacw.reconciliation.dao.UserMapper;
import com.bigdata.sacw.reconciliation.entity.User;
import com.bigdata.sacw.reconciliation.service.IUserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {
}
