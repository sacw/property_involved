package com.bigdata.sacw.reconciliation;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
@Slf4j
@MapperScan("com.bigdata.sacw.reconciliation.dao")
public class ReconciliationApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReconciliationApplication.class, args);
        System.out.println("---------------------------------------------");
        System.out.println("------ReconciliationApplication is run !!----");
        System.out.println("---------------------------------------------");
    }

}
