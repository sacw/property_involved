package com.bigdata.sacw.reconciliation.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bigdata.sacw.reconciliation.entity.User;

public interface UserMapper extends BaseMapper<User> {
}
