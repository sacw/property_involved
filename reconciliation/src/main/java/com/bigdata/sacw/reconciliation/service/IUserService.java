package com.bigdata.sacw.reconciliation.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bigdata.sacw.reconciliation.entity.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;

public interface IUserService extends IService<User> {

}
